test('Copy object', () => {
  const target = {};
  const src = {
    a: {
      b: {
        c: [1, 2],
        d: 9,
      },
      e: true,
    },
  };
  expect(Object.assign(target, src)).toEqual(src);
});

test('Target object mutation', () => {
  const target = { a: 1 };
  const src = { a: 3 };

  expect(Object.assign(target, src)).toBe(target);
});

test('Rewrite property and merge', () => {
  const target = { a: 1 };
  const src = [
    { a: 2, b: 10 },
    { a: 3, b: 11, c: 100 },
  ];
  const expected = { a: 3, b: 11, c: 100 };
  expect(Object.assign(target, ...src)).toEqual(expected);
});

test('Rewrite property', () => {
  const target = { a: 1 };
  const src = [{ a: 2 }, { a: 3 }];
  const expected = { a: 3 };
  expect(Object.assign(target, ...src)).toEqual(expected);
});

test('Empty objects', () => {
  const target = {};
  const src = [{}, {}];
  const expected = {};
  expect(Object.assign(target, ...src)).toEqual(expected);
});

test('Rewrite readonly property', () => {
  const target = Object.defineProperty({}, 'c', {
    value: 'readonly',
    writable: false,
  });
  const src = [{ c: 'injection' }];

  expect(() => Object.assign(target, ...src)).toThrowError(TypeError);
});

test('Empty params', () => {
  expect(() => Object.assign()).toThrowError(TypeError);
});

test('String as target', () => {
  const target = {};
  const src = 'Hello world';

  const expected = {
    0: 'H',
    1: 'e',
    2: 'l',
    3: 'l',
    4: 'o',
    5: ' ',
    6: 'w',
    7: 'o',
    8: 'r',
    9: 'l',
    10: 'd',
  };

  expect(Object.assign(target, src)).toEqual(expected);
});
